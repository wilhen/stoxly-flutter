import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import './stock_detail.dart' as stock_detail;


class SearchData {
  final String ticker;
  final String companyName;
  
  
  const SearchData({this.ticker, this.companyName});

}

class SearchList extends StatelessWidget{
  final List<SearchData> datas;
  SearchList(this.datas);
  @override
  Widget build(BuildContext context) {
    return new ListView(
      // type: MaterialListType.twoLine,
      padding: new EdgeInsets.symmetric(vertical: 8.0),
      children: _buildContactList()
    );
  }

  List<SearchCard> _buildContactList() {
    List <SearchCard> tempListItem= <SearchCard>[];
   
    for (SearchData data in datas){
      SearchListItem trendingListItem = SearchListItem(data);

      tempListItem.add(SearchCard(trendingListItem, data));
    } 
    return tempListItem;
  }
}

class SearchCard extends StatelessWidget {
  final SearchListItem _data;
  final SearchData data;
  SearchCard(this._data, this.data);

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      onTap:(){
        print("search tapped");
        Navigator.push(context, new MaterialPageRoute(
          builder: (context)=>new stock_detail.StockDetail(sym:data.ticker, companyName: data.companyName),
        ));
      },
      child:new Card(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            _data,
            // new Container(
            //   margin: const EdgeInsets.only(top: 1.00, bottom: 1.00),
            // ),
          ]
        )
      )
    );
    
  }

}

class SearchListItem extends StatelessWidget {
  final SearchData _data;

  SearchListItem(this._data);

  @override
  Widget build(BuildContext context) {
    return new ListTile(
      // leading: new Icon(Icons.map),
      title: new Text(
        _data.ticker,
        style: new TextStyle(fontWeight: FontWeight.bold),
        ),
      subtitle: new Text(_data.companyName),
    );
  }

}


// class BodyElement extends StatelessWidget {
//   List<SearchData> data=[];
//   final Future<http.Response> corresponder;
//   BodyElement (this.corresponder);
//   @override
//   Widget build(BuildContext context) {
//     new FutureBuilder(
//       future: corresponder,
//       builder: (BuildContext context, AsyncSnapshot<http.Response> res){
//         if (res.hasData){
//           List datas = (JSON.decode(res.data.body))["Results"];
//           data  = <SearchData>[];
          
//           for(Map d in datas)
//             data.add(SearchData(companyName: d["Text"], ticker: d["Value"]));

//           return new SearchList(data);
//         }
//         return new Container();
//       }
//     );
//   }
// }

class Spinner extends StatelessWidget{
  @override
  Widget build(BuildContext context){
     return new Container(
        child: Center(
          child: new CircularProgressIndicator(),
        )
      ); 
  }
}

class SubSearch extends StatefulWidget{
  SubSearch({Key key}) : super(key: key);
  @override
  _SubSearch createState() => new _SubSearch();
  
}

class _SubSearch extends State<SubSearch>{
  final myController = new TextEditingController();
 
  List <SearchData> data  = <SearchData>[];
  void initState(){
    super.initState();
    myController.addListener(_printLatestValue);
  }

  _printLatestValue() {
    print("Second text field: ${myController.text}");
    if (myController.text != "" ){
      Future<http.Response> res= getSearchData(myController.text);
     
      res.then((result) {
        if (result != null){
          data = <SearchData>[];
          
          List datas = (JSON.decode(result.body))["Results"];
          // print ((JSON.decode(result.body))["Results"]);
          if (this.mounted){
            setState(() {

              for(Map d in datas){
                
                String t = d["Text"];
                
                if (t.length>35){
                  t = t.substring(0,35);
                  t = t+"...";
                }
                data.add(SearchData(companyName: t, ticker: d["Value"]));
              }
              return new SearchList(data);        
            });
          }
        }
      
      });
    }
    
  }

  @override
  void dispose() {
    
    // Clean up the controller when the Widget is removed from the Widget tree
    myController.removeListener(_printLatestValue);
    myController.dispose();
    super.dispose();
  }

  Future<http.Response> getSearchData(symbol) async{
    http.Response response = await http.get(
      "https://search.xignite.com/Search/Suggest?parameter=XigniteGlobalQuotes.GetGlobalDelayedQuote.Identifier&term=$symbol",
      headers: {
        "Accept":"application/json"
      }
    );
    
    return response;
    
  }

  @override
  Widget build(BuildContext context) {
    // getSearchData("BB");
    
    return new Scaffold(
      appBar: new AppBar( 
        title: new TextField(
          decoration: new InputDecoration(
            
            hintText: "Search",
            hintStyle: new TextStyle(
              color:Colors.white
            ),
            border: InputBorder.none,
            fillColor: Colors.white
          ),
          style: new TextStyle(
            color:Colors.white
          ),
          controller: myController,
          // onChanged: (text) {
            // print("First text  field: $text");
          // },
        )
      ),
      body:   new SearchList(data)
      
    );
  }
}