import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import './stock_detail.dart' as stock_detail;

class Trending {
  final String ticker;
  final String companyName;
  double price;
  String change;
  String sentiment;
  Trending({this.ticker, this.companyName, this.price, this.change, this.sentiment});

  void setPrice(double price){
    this.price = price;

  }

  void setChange(String change){
    this.change = change;
  }

  void setSentiment(String sentiment){
    this.sentiment = sentiment;
  }

}

class TrendingList extends StatelessWidget{
  final List<Trending> trendings;
  TrendingList(this.trendings);
  @override
  Widget build(BuildContext context) {
    return new ListView(
      // type: MaterialListType.twoLine,
      padding: new EdgeInsets.symmetric(vertical: 8.0),
      children: _buildContactList()
    );
  }

  List<TrendingCard> _buildContactList() {
    List <TrendingCard> tempListItem= <TrendingCard>[];
   
    for (Trending trending in trendings){
      TrendingListItem trendingListItem = TrendingListItem(trending);

      tempListItem.add(TrendingCard(trendingListItem, trending));
    } 
    return tempListItem;
  }
}
class TrendingListItem extends StatelessWidget {
  final Trending _trending;

  TrendingListItem(this._trending);

  @override
  Widget build(BuildContext context) {
    return new ListTile(
      // leading: new Icon(Icons.map),
      title: new Text(
        _trending.ticker,
        style: new TextStyle(fontWeight: FontWeight.bold),
        ),
      subtitle: new Text(_trending.companyName),
    );
  }

}

class ReturnColorCode extends StatelessWidget{
  final double change;
  ReturnColorCode(this.change);
  @override
  Widget build(BuildContext context) {
    if (change<=0){
      String changeString = change.toString();
      return new Text(
        "$changeString%",
        style: new TextStyle( fontSize: 30.0, color: Colors.red),
      );
    }else{
      String changeString = change.toString();
      return new Text(
        "$changeString%",
        style: new TextStyle( fontSize: 30.0, color: Colors.green),
      );
    }
  }
}

class TrendingCard extends StatelessWidget {
  final TrendingListItem _trending;
  final Trending trending;
  TrendingCard(this._trending, this.trending);

  Future<http.Response> getIntraDay() async{
    String symbol = trending.ticker;
    http.Response response = await http.get(
      "http://35.189.14.118/quote/$symbol",
      headers: {
        "Accept":"application/json"
      }
    );
    print (JSON.decode(response.body));
    return response;
  }


  @override
  Widget build(BuildContext context) {
    
    return new FutureBuilder(
      future: getIntraDay(),
      builder: (BuildContext context, AsyncSnapshot<http.Response> res){
        if (res.hasData){
            Map data = JSON.decode(res.data.body);
            double open = data["1. open"];
            double close = data["4. close"];
            String vol = data["5. volume"];
            double change = data["6. change"];
            
            
            trending.setChange(change.toString());
            trending.setPrice(close);
            trending.setSentiment(vol);
            return new GestureDetector(
              onTap: (){
                print ("tapped this out");
                Navigator.push(context, new MaterialPageRoute(
                  builder: (context)=>new stock_detail.StockDetail(sym:trending.ticker, companyName: trending.companyName),
                ));
              },
              child: new Card(
                child: new Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    _trending,
                    new Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                          child:  new Column(
                            children: <Widget>[
                              new Row(
                                  children: <Widget>[
                                      new Text(
                                        "Change",
                                        style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)
                                      )
                                  ],
                              ),
                              new Row(
                                children: <Widget>[
                                  new ReturnColorCode(change)
                                ],
                              )
                              
                            ],
                          ),
                        ),
                      
                        // new Padding(
                        //   padding: new EdgeInsets.all(5.0),
                        // ),
                        new Container(
                          height: 55.0,
                          width: 6.0,
                          color: Colors.transparent,
                          margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        ),
                        new Container(
                          child: new Column(
                            children: <Widget>[
                              new Row(
                                children: <Widget>[
                                    new Text(
                                      "Price",
                                      style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)
                                    )
                                ],
                              ),
                              new Row(
                                children: <Widget>[
                                  new Text(
                                    "\$"+trending.price.toString(),
                                    style: new TextStyle( fontSize: 30.0, color: Colors.blueAccent)
                                  ),
                                ],
                              ),
                            
                            ],
                          ),
                        ),
                        
                        // new Container(
                        //   height: 55.0,
                        //   width: 6.0,
                        //   color: Colors.transparent,
                        //   margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                        // ),
                        // new Column(
                        //   children: <Widget>[
                        //     new Row(
                        //       children: <Widget>[
                        //           new Text(
                        //             "Volume",
                        //             style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)
                        //           )
                        //       ],
                        //     ),
                        //     new Row(
                        //       children: <Widget>[
                        //         new Text(
                        //           trending.sentiment.toString(),
                        //           style: new TextStyle( fontSize: 30.0, color: Colors.blueAccent),
                        //         )
                        //       ],
                        //     )
                            
                        //   ],
                        // )
                      ],
                    ),
                    new Container(
                      margin: const EdgeInsets.only(top: 5.00, bottom: 5.00),
                    ),
                  ]
                )
              )
            );
            
          }
        return new Container();
      }
    );
    
  }

}

class Spinner extends StatelessWidget{
  @override
  Widget build(BuildContext context){
     return new Container(
        child: Center(
          child: new CircularProgressIndicator(),
        )
      ); 
  }
}

class Search extends StatelessWidget{
  List<Trending> _trendings = [];

  Future<http.Response> getTrendingData() async{
    http.Response response = await http.get(
      "http://35.189.14.118/trending",
      headers: {
        "Accept":"application/json"
      }
    );
    return response;
    
  }
  
  @override
  Widget build(BuildContext context) {
      

    return new Scaffold(
      
      body: new FutureBuilder(
        future: getTrendingData(),
        builder: (BuildContext context, AsyncSnapshot<http.Response> res){

          if (res.hasData){
             List data = JSON.decode(res.data.body);
            _trendings = <Trending>[];
          
            for (Map d in data){
              String title = d["title"];
              if (title.length>35){
                title = title.substring(0,35);
                title = title+"...";
              }
              _trendings.add(Trending(change: "0.05%", companyName: title, ticker: d["symbol"], price: 100.00, sentiment: "0.59"));
            }
              
          
            return new TrendingList(_trendings);
          }
          return new Spinner();
        }
      )
    );
  }
}
