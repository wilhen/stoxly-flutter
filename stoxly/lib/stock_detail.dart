import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'dart:math' as math;
import "./dashboard.dart" as Dashboard;



class Spinner extends StatelessWidget{
  @override
  Widget build(BuildContext context){
     return new Container(
        child: Center(
          child: new CircularProgressIndicator(),
        )
      ); 
  }
}

class StockDetail extends StatefulWidget{
  final String sym;
  final String companyName;
  StockDetail({Key key, this.sym, this.companyName}) : super(key: key);
  @override
  _StockDetail createState() => new _StockDetail(this.sym, this.companyName);
  
}

/// Sample time series data type.
class TimeSeriesStocks {
  final DateTime time;
  final double price;

  TimeSeriesStocks(this.time, this.price);
}


class StockChange extends StatelessWidget{
  double change;
  final double percent;
  double price;

  StockChange(this.price, this.change, this.percent);
  @override
  Widget build(BuildContext context){
    
    if (percent<=0){
      price =  (num.parse(price.toStringAsFixed(2))).toDouble();
      return new Container(
        
        child: new Row(
          children: <Widget>[
            new Text("\$$price", style: TextStyle( fontSize: 24.5, fontWeight: FontWeight.w100)),
            new Padding(
              padding: EdgeInsets.all(3.0),
            ),
            new Icon(Icons.arrow_downward, color: Colors.red,size: 24.00,),
            new Text("$percent%", style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w100, color: Colors.red),),
            new Padding(
              padding: EdgeInsets.all(1.0),
            ),
            new Text("($change)", style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w100, color: Colors.red),)
          ],
        )
      );
    }else{
      return new Container(
        child: new Row(
          children: <Widget>[
            new Text("\$$price", style: TextStyle( fontSize: 24.5, fontWeight: FontWeight.w100)),
            new Padding(
              padding: EdgeInsets.all(3.0),
            ),
            new Icon(Icons.arrow_upward, color: Colors.green,size: 24.00,),
            new Text("$percent%", style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w100, color: Colors.green),),
            new Padding(
              padding: EdgeInsets.all(1.0),
            ),
            new Text("($change)", style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w100, color: Colors.green),)
          ],
        )
      );
    }
  
  }

}


class ButtonActive extends StatelessWidget{
  final String label;
  final String selectedLabel;
  ButtonActive(this.label, this.selectedLabel);

  @override
  Widget build(BuildContext context){
    if (label!= selectedLabel){
      return new Text(label, style: new TextStyle(color: Colors.blueAccent, fontSize: 18.0, fontWeight: FontWeight.w300),);
    }else{
      return new Text(label, style: new TextStyle(color: Colors.blueAccent, fontSize: 18.0, fontWeight: FontWeight.w700),);
    }
    
  }
}



class _StockDetail extends State<StockDetail>{
  List<charts.Series<TimeSeriesStocks, DateTime>> seriesList =[];
  
  final String sym ;
  final String companyName;
  bool updated = false;
  bool added = false;
  String active = "1D";

  int _counter = 0;
  
  final GlobalKey<AnimatedCircularChartState> _chartKey = new GlobalKey<AnimatedCircularChartState>();
  
  _StockDetail(this.sym, this.companyName);

  void initState(){
    super.initState();
   
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _cycleSamples() {
    List<CircularStackEntry> nextData = <CircularStackEntry>[
      new CircularStackEntry(
        <CircularSegmentEntry>[
          new CircularSegmentEntry(75.0, Colors.red[200], rankKey: 'negatives'),
          new CircularSegmentEntry(131.0, Colors.green[200], rankKey: "positives"),
          // new CircularSegmentEntry(2000.0, Colors.blue[200], rankKey: 'Q3'),
          // new CircularSegmentEntry(1000.0, Colors.yellow[200], rankKey: 'Q4'),
        ],
        rankKey: 'Daily Sentiment',
      ),
    ];
    setState(() {
      _chartKey.currentState.updateData(nextData);
    });
  }


  Future<http.Response> getStockData(symbol, frequency) async{
    http.Response response = await http.get(
      "http://35.189.14.118/analytics/$symbol",
      headers: {
        "Accept":"application/json"
      }
    );
    
    return response;
  }

  /// Create one series with sample hard coded data.
  List<charts.Series<TimeSeriesStocks, DateTime>> _createSampleData() {
    final data = [
      new TimeSeriesStocks(new DateTime(2017, 9, 19), 100.0),
      new TimeSeriesStocks(new DateTime(2017, 9, 26), 101.0),
      new TimeSeriesStocks(new DateTime(2017, 10, 3), 99.0),
      new TimeSeriesStocks(new DateTime(2017, 10, 10), 98.7),
    ];

    return [
      new charts.Series<TimeSeriesStocks, DateTime>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesStocks sales, _) => sales.time,
        measureFn: (TimeSeriesStocks sales, _) => sales.price,
        data: data,
      )
    ];
  }

  List<charts.Series<TimeSeriesStocks, DateTime>> _generateStockChart(timeSeries){
    return [
      new charts.Series<TimeSeriesStocks, DateTime>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesStocks sales, _) => sales.time,
        measureFn: (TimeSeriesStocks sales, _) => sales.price,
        data: timeSeries,
      )
    ];
  }

  Future<http.Response> generateChartData(symbol, fq) async{
    
    http.Response response = await http.get(
      "http://35.189.14.118/prices/$symbol?frequency=$fq",
      headers: {
        "Accept":"application/json"
      }
    );
 
    return response;
  }

  generateChart(symbol, fq){
    Future<http.Response> res = generateChartData(symbol,fq);
    res.then((result) {
      if (result!=null){
      
        List data = JSON.decode(result.body);
        // print (data);
        // print ("cant touch me");
        List<TimeSeriesStocks> chartData = [];
        for (Map ch in data){
          double closeSeries = ch["close"];
          
          chartData.add(new TimeSeriesStocks(DateTime.parse(ch["date"]), closeSeries));
        }
         
        setState(() {
          this.seriesList = [
            new charts.Series<TimeSeriesStocks, DateTime>(
              id: 'Sales',
              colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
              domainFn: (TimeSeriesStocks sales, _) => sales.time,
              measureFn: (TimeSeriesStocks sales, _) => sales.price,
              data: chartData,
            )
          ]; 
          if (fq=="daily"){
            active="1D";
            
          }else if (fq=="weekly"){
            active="5D";
          }else{
            active="1M";
          }


          this.updated = true;
        });
     
      }
    });
  }

  Widget getSentimentDonut(positives, negatives){
    if (positives >0 || negatives >0){
      List<CircularStackEntry> sentiments = <CircularStackEntry>[
        new CircularStackEntry(
          <CircularSegmentEntry>[
            new CircularSegmentEntry((positives).toDouble(), Colors.green[200], rankKey: 'positives'),
            new CircularSegmentEntry((negatives).toDouble(), Colors.red[200], rankKey: "negatives"),
          ],
          rankKey: 'Daily Sentiment',
        ),
      ];
      return new Center(
        child:  new AnimatedCircularChart(
          key: _chartKey,
          size: const Size(300.0, 300.0),
          initialChartData: sentiments,
          chartType: CircularChartType.Radial,
          // percentageValues: true,
          holeLabel: (math.log((1+positives)/(1+negatives))).toStringAsPrecision(2),
          labelStyle: new TextStyle(
            color: Colors.blueGrey[600],
            fontWeight: FontWeight.bold,
            fontSize: 24.0,
          ),
        )  
      );
    }else{
      return new Container(
        padding: EdgeInsets.all(30.0),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.center,
          
          children: <Widget>[
           
            new Center(
              
              child: Text("No Tweets collected with this symbol", style: TextStyle(fontWeight: FontWeight.w500, color: Colors.grey),),
            ) 
          ],
         )
      );
    }
    
  }


  Future<http.Response> getShortlist() async{
    String accessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjY1NDU3MjQsIm5iZiI6MTUyNjU0NTcyNCwianRpIjoiODBhZGY1NGEtNDk4ZC00YTJkLTg3MDMtMWM5YWQ3NjlmYmZlIiwiZXhwIjoxNTI5MTM3NzI0LCJpZGVudGl0eSI6IjI1OWNhN2QxNzViNTQxNWViYjlhODFlYzA4ODE4M2Y5IiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIiwiY3NyZiI6IjBhMjU1MzY2LTEwNmMtNGMzOC1hOTE5LWU4MDYyODM1ZDJmZiJ9.twl7lneVYXWf_3P2ZziLyz2L_xDb6KmAaYoUQzgYhE8";

    http.Response response = await http.get(
      "http://35.189.14.118/api/shortlist",
      headers: {
        "Accept":"application/json",
        "Authorization": accessToken
      }
    );
    
    return response;
  }

  Future<http.Response> add() async{
    String accessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjY1NDU3MjQsIm5iZiI6MTUyNjU0NTcyNCwianRpIjoiODBhZGY1NGEtNDk4ZC00YTJkLTg3MDMtMWM5YWQ3NjlmYmZlIiwiZXhwIjoxNTI5MTM3NzI0LCJpZGVudGl0eSI6IjI1OWNhN2QxNzViNTQxNWViYjlhODFlYzA4ODE4M2Y5IiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIiwiY3NyZiI6IjBhMjU1MzY2LTEwNmMtNGMzOC1hOTE5LWU4MDYyODM1ZDJmZiJ9.twl7lneVYXWf_3P2ZziLyz2L_xDb6KmAaYoUQzgYhE8";
    Map<String, String> data = {"ticker": this.sym};
    String jsonData = JSON.encode(data);
    print (jsonData);
    http.Response response = await http.post(
      "http://35.189.14.118/shortlist",
      headers: {
        "Accept":"application/json",
        "Content-Type":"application/json",
        "Authorization": accessToken
      },
      body: jsonData
    );
    print (response.body);
    return response;
  }

  Future<http.Response> delete() async{
    String accessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjY1NDU3MjQsIm5iZiI6MTUyNjU0NTcyNCwianRpIjoiODBhZGY1NGEtNDk4ZC00YTJkLTg3MDMtMWM5YWQ3NjlmYmZlIiwiZXhwIjoxNTI5MTM3NzI0LCJpZGVudGl0eSI6IjI1OWNhN2QxNzViNTQxNWViYjlhODFlYzA4ODE4M2Y5IiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIiwiY3NyZiI6IjBhMjU1MzY2LTEwNmMtNGMzOC1hOTE5LWU4MDYyODM1ZDJmZiJ9.twl7lneVYXWf_3P2ZziLyz2L_xDb6KmAaYoUQzgYhE8";
    Map<String, String> data = {"ticker": this.sym};
    String jsonData = JSON.encode(data);
    print (jsonData);
    http.Response response = await http.post(
      "http://35.189.14.118/shortlist/delete",
      headers: {
        "Accept":"application/json",
        "Content-Type":"application/json",
        "Authorization": accessToken
      },
      body: jsonData
    );
    print (response.body);
    return response;
  }


  Widget printPrediction(double predictionPrice){
    if (predictionPrice>0){
      return new Text("\$$predictionPrice", style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.w200),);
    }else{
      return new Text("Not Available", style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.w200, color: Colors.grey,));
    }
  }

  MaterialColor getColorCode(double value){
    if(value<=0){
      return Colors.red;
    }else{
      return Colors.green;
    }
  }
 

  @override
  Widget build(BuildContext context) {
   

    return new Scaffold(
      appBar: new AppBar( 
        title: new Text(sym)
      ),
      body: new FutureBuilder(
        future: getStockData(this.sym, "daily"),
        builder:(BuildContext context, AsyncSnapshot<http.Response> res){
          if (res.hasData){
             Map aggregateData = JSON.decode(res.data.body);
            //  print (aggregateData["latest_quote"]);
            //  print (aggregateData["sentiment"]);
             List<TimeSeriesStocks> chartData = [];
             for (Map ch in aggregateData["charts"]){
               double closeSeries = ch["close"];
               
               chartData.add(new TimeSeriesStocks(DateTime.parse(ch["date"]), closeSeries));
             }
            //  print (this.updated);
           
             if (!this.updated){
               this.seriesList = _generateStockChart(chartData);
             }
             
             double predictedPrice = aggregateData["prediction_price"];
             predictedPrice = (num.parse(predictedPrice.toStringAsFixed(2))).toDouble();
             double change = (aggregateData["latest_quote"]["close"] - aggregateData["latest_quote"]["open"]);
             double c = (num.parse(change.toStringAsFixed(2))).toDouble();
             int positives = aggregateData["sentiment"]["positives"];
             int negatives = aggregateData["sentiment"]["negatives"];
             double price = aggregateData["latest_quote"]["close"];
             double predicted_return = aggregateData["prediction_return"];
             predicted_return = (num.parse(predicted_return.toStringAsFixed(2))).toDouble();
            //  print(predictedPrice);
             print (aggregateData);
           
            //  return new Aggregation(this.sym, this.companyName, this.seriesList, aggregateData["sentiment"]["positives"], aggregateData["sentiment"]["negatives"],aggregateData["latest_quote"]["change"],c, price);
            return new SingleChildScrollView(
              padding: new EdgeInsets.all(20.0),
              child:  new Container(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(this.companyName, style: TextStyle(fontWeight: FontWeight.w500, fontSize: 30.0)),
                    new Padding(
                      padding: EdgeInsets.all(2.5),
                    ),
                    StockChange(price, c, aggregateData["latest_quote"]["change"]),
                    new Padding(
                      padding: EdgeInsets.all(10.0),
                    ),
                    new Container(
                      padding: EdgeInsets.all(1.0),
                      child: new charts.TimeSeriesChart(this.seriesList,
                        animate: true,
                        // Optionally pass in a [DateTimeFactory] used by the chart. The factory
                        // should create the same type of [DateTime] as the data provided. If none
                        // specified, the default creates local date time.
                        dateTimeFactory: const charts.LocalDateTimeFactory(),
                      ),
                      height: 200.0,
                      width:350.0
                    ),
                    new Padding(
                      padding: EdgeInsets.all(10.0),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Column(children: <Widget>[
                          new GestureDetector(
                            child: ButtonActive("1D",active),
                            onTap: (){
                              generateChart(this.sym,"daily");
                            }
                          ),
                          
                        ],),
                        new Column(children: <Widget>[
                          new GestureDetector(
                            child:  ButtonActive("5D",active),
                            onTap: (){
                              generateChart(this.sym,"weekly"); 
                            }
                          )
                        ],),
                        new Column(children: <Widget>[
                          new GestureDetector(
                            child: ButtonActive("1M",active),
                            onTap: (){
                              generateChart(this.sym,"monthly");
                             
                              // print ("1M");
                            }
                          ),
                          
                        ],),
                      ],
                    ),
                    new Padding(
                      padding: EdgeInsets.all(10.0),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                     
                      children: <Widget>[
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text("Predicted Price", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600),),
                          ],
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            // new Text("\$$predictedPrice", style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.w200),)
                            printPrediction(predictedPrice)
                          ],
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text("At closing", style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w200, color: Colors.grey),)
                           
                          ],
                        ),
                      ],),
                      new Padding(padding: EdgeInsets.all(6.0),),
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text("Predicted Return", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600),),
                          ],
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text(predicted_return.toString(), style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.w200, color: getColorCode(predicted_return)),)
                           
                          ],
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text("At closing", style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w200, color: Colors.grey),)
                           
                          ],
                        ),
                      ],)
                    ],),

                    new Padding(padding:  EdgeInsets.all(10.0),),
                    new Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new FlatButton(
                        
                          colorBrightness: Brightness.light,
                          child: new Text("Dashboard", style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w500)),
                          textColor: Colors.blue,
                          onPressed: (){
                            Navigator.of(context).pushNamed("/dashboard");
                          },
                        ),
                      ],
                    ),
                    new Padding(
                      padding: EdgeInsets.all(10.0),
                    ),
                    new Row(children: <Widget>[
                      new Text("Sentiment Analysis", style: new TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.w500, fontSize: 25.0),)
                    ],),
                    new Padding(
                      padding: EdgeInsets.all(10.0),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Column(children: <Widget>[
                          new Text("Positive", style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.w600),),
                          
                        ],),
                        new Padding(
                          padding: EdgeInsets.all(5.0),
                        ),
                        new Column(
                          children: <Widget>[
                            new Text("Negative", style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.w600),)
                          ],
                        )
                        
                      ],
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Column(children: <Widget>[
                          new Text("$positives", style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.w200),),
                          
                        ],),
                        new Padding(
                          padding: EdgeInsets.all(5.0),
                        ),
                        new Column (children: <Widget>[
                          new Text("$negatives", style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.w200),)
                        ],)
                        
                      ],
                    ),
                    // the donut graph
                    new Container(padding: EdgeInsets.all(5.0)),
                    getSentimentDonut( positives, negatives),
                  ])

                )
            );
          }
          return Spinner();
        }
        
      ),
      
     
      floatingActionButton: new FutureBuilder(
        future: getShortlist(),
        builder: (BuildContext context, AsyncSnapshot<http.Response> res){
          if (res.hasData){
            
            Map data = JSON.decode(res.data.body);
            List symbols = data["symbols"];
             // clean up list
            // print (JSON.decode(res.data.body));
            bool exist = false;
            for (Map symbol in symbols){
              String companyName = symbol["company_name"];
              String symbolString = symbol["symbol"];
              if(symbolString == this.sym){
                exist=true;
                break;

              }
            }

            if (exist){
              added = true;
              return new FloatingActionButton(
                  // onPressed: _incrementCounter,
                  onPressed:() {
                    print ("added");
                    Future<http.Response> resp = delete();
                    resp.then((message){
                      if (message != null){
                        int code = message.statusCode;
                       
                        Map msgFeedback = JSON.decode(message.body);
                        String msg = msgFeedback["message"];
                        
                        if (code == 201){
                          setState(() {
                            added = false;
                          });  
                        } 
                        var snackBar = new SnackBar(
                          content: new Text(msg),
                          action: null
                        );    

                        Scaffold.of(context).showSnackBar(snackBar);                  
                          
                        
                      }
                      return new Spinner();
                    });
                    
                     
                  },
                  tooltip: 'Increment',
                  child: new Icon(Icons.favorite),
                );
            }
                
            return new FloatingActionButton(
              // onPressed: _incrementCounter,
              onPressed: (){

                print ("from not added");
                Future<http.Response> resp = add();
                resp.then((message){
                  if (message != null){
                    
                    Map msgFeedback = JSON.decode(message.body);
                    String msg = msgFeedback["message"];
                    
                    if (message.statusCode == 201){
                      setState(() {
                        added = true; 
                      });
                    }
                    var snackBar = new SnackBar(
                      content: new Text(msg),
                      action: null
                    );  
                    Scaffold.of(context).showSnackBar(snackBar);               
                    
                  }
                  
                  return Spinner();
                });
              },
              tooltip: 'Increment',
              child: new Icon(Icons.favorite_border),
            );
          }  
          return new Container();
        }
        
      )
      
     
      

    );
  }
}