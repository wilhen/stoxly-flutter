import 'package:flutter/material.dart';
import './search.dart' as search;
import './sub_search.dart' as sub_search;
import './stock_detail.dart' as stock_detail;
import './settings.dart' as settings;
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:math';
import './dashboard.dart' as dashboard;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back tos zero; the application is not restarted.
        primarySwatch: Colors.blueGrey,
      ),
      home: new MyHomePage(title: 'Stoxly'),
      routes: <String,WidgetBuilder>{
        "/search":(BuildContext context)=> new sub_search.SubSearch(),
        "/detail":(BuildContext context)=> new stock_detail.StockDetail(),
        "/dashboard":(BuildContext context)=> new dashboard.Dashboard()
      },
    );
  }
}

class Spinner extends StatelessWidget{
  @override
  Widget build(BuildContext context){
     return new Container(
        child: Center(
          child: new CircularProgressIndicator(),
        )
      ); 
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}


class Shortlist {
  final String ticker;
  final String companyName;
  double price;
  double change;
  double sentiment;
  Shortlist({this.ticker, this.companyName, this.price, this.change, this.sentiment});

  setPrice(double price){
    this.price = price;
  }
  setChange(double change){
    this.change = change;
  }

  setSentiment(double sentiment){
    this.sentiment = sentiment;
  }

}


class ShortlistList extends StatelessWidget{
  List<Shortlist> shortlists;
  ShortlistList(this.shortlists);

  Future<http.Response> getShortlist() async{
    String accessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjY1NDU3MjQsIm5iZiI6MTUyNjU0NTcyNCwianRpIjoiODBhZGY1NGEtNDk4ZC00YTJkLTg3MDMtMWM5YWQ3NjlmYmZlIiwiZXhwIjoxNTI5MTM3NzI0LCJpZGVudGl0eSI6IjI1OWNhN2QxNzViNTQxNWViYjlhODFlYzA4ODE4M2Y5IiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIiwiY3NyZiI6IjBhMjU1MzY2LTEwNmMtNGMzOC1hOTE5LWU4MDYyODM1ZDJmZiJ9.twl7lneVYXWf_3P2ZziLyz2L_xDb6KmAaYoUQzgYhE8";

    http.Response response = await http.get(
      "http://35.189.14.118/api/shortlist",
      headers: {
        "Accept":"application/json",
        "Authorization": accessToken
      }
    );
    
    return response;
  }

  @override
  Widget build(BuildContext context) {
   

    return new FutureBuilder(
      future: getShortlist(),
      builder: (BuildContext context, AsyncSnapshot<http.Response> res){
        if (res.hasData){
          Map data = JSON.decode(res.data.body);
          List symbols = data["symbols"];
          shortlists = []; // clean up list
          print (JSON.decode(res.data.body));

          for (Map symbol in symbols){
            String companyName = symbol["company_name"];
            String symbolString = symbol["symbol"];
            shortlists.add(Shortlist(change: null, companyName: companyName, ticker: symbolString, price: null, sentiment: null));
          }
          return new ListView(
            // type: MaterialListType.twoLine,
            padding: new EdgeInsets.symmetric(vertical: 8.0),
            children: _buildContactList()
          );
        }
        return new Spinner();
      }
    );
  }

  List<ShortlistCard> _buildContactList() {
    List <ShortlistCard> tempListItem= <ShortlistCard>[];
   
    for (Shortlist shortlist in shortlists){
      ShortlistListItem shortlistListItem = ShortlistListItem(shortlist);

      tempListItem.add(ShortlistCard(shortlistListItem, shortlist));
    } 
    return tempListItem;
  }
}


class ShortlistListItem extends StatelessWidget {
  final Shortlist _shortlist;

  ShortlistListItem(this._shortlist);

  @override
  Widget build(BuildContext context) {
    return new ListTile(
      // leading: new Icon(Icons.map),
      title: new Text(
        _shortlist.ticker,
        style: new TextStyle(fontWeight: FontWeight.bold),
        ),
      subtitle: new Text(_shortlist.companyName),
    );
  }

}

class ShortlistCard extends StatelessWidget {
  final ShortlistListItem _shortlist;
  final Shortlist shortlist;
  ShortlistCard(this._shortlist, this.shortlist);

  Future<http.Response> getShortlistDetail() async{
    String symbol = this.shortlist.ticker;
    http.Response response = await http.get(
      "http://35.189.14.118/shortlist/aggregation/$symbol",
      headers: {
        "Accept":"application/json"
      }
    );
    print (JSON.decode(response.body));
    return response;
  }

  Widget getColorCodeChange(double change){
    if (change>0){
      return new Text(
        "$change%",
        style: new TextStyle( fontSize: 27.0, color: Colors.green),
      );
    }else{
      return new Text(
        "$change%",
        style: new TextStyle( fontSize: 27.0, color: Colors.redAccent),
      );
    }

  }

  Widget getColorCodeSentiment(double sentiment){
    if (sentiment>0){
      return new Text(
        sentiment.toString(),
        style: new TextStyle( fontSize: 27.0, color: Colors.green),
      );
    }else{
      return new Text(
        sentiment.toString(),
        style: new TextStyle( fontSize: 27.0, color: Colors.redAccent),
      );
    }
  }

  Widget printPredictionPrice(double price){
    if (price>0){
      return new Text(
        "\$"+price.toString(),
        style: new TextStyle( fontSize: 27.0, color: Colors.blueAccent)
      );
    }else{
      new Text(
          "N/A",
          style: new TextStyle( fontSize: 27.0, color: Colors.blueAccent)
        );
    }
    
  }
  @override
  Widget build(BuildContext context) {
    

    return new FutureBuilder(
      future: getShortlistDetail(),
      builder: (BuildContext context, AsyncSnapshot<http.Response> res){
        if (res.hasData){
          Map data = JSON.decode(res.data.body);
          double prediction = (data["price_prediction"]);
          prediction = (num.parse(prediction.toStringAsFixed(2))).toDouble();
          this.shortlist.setPrice(prediction);
          double change = data["latest_quote"]["change"];
          change = (num.parse(change.toStringAsFixed(2))).toDouble();
          // String changeString = change.toString()+"%";
          this.shortlist.setChange(change);
          // double sentiment = (data["sentiment"]["positives"] - data["sentiment"]["negatives"]) /(data["sentiment"]["positives"] + data["sentiment"]["negatives"]);
          double sentiment = log((data["sentiment"]["positives"]+1)/(data["sentiment"]["negatives"]+1));
          double s = (num.parse(sentiment.toStringAsFixed(2))).toDouble();
          this.shortlist.setSentiment(s);
          return new 
          GestureDetector(
            onTap: (){
              print (shortlist.ticker);
              Navigator.push(context, new MaterialPageRoute(
                builder: (context)=>new stock_detail.StockDetail(sym:shortlist.ticker, companyName: shortlist.companyName),
              ));
            },
            child:  Card(
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _shortlist,
                  new Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Container(
                        child:    new Column(
                          children: <Widget>[
                            new Row(
                                children: <Widget>[
                                    new Text(
                                      "Change",
                                      style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)
                                    )
                                ],
                            ),
                            new Row(
                              children: <Widget>[
                                getColorCodeChange(shortlist.change)
                              ],
                            )
                            
                          ],
                        ),
                      ),
                   
                      // new Padding(
                      //   padding: new EdgeInsets.all(5.0),
                      // ),
                      new Container(
                        height: 55.0,
                        width: 6.0, //6
                        color: Colors.transparent,
                        margin: const EdgeInsets.only(left: 8.0, right: 8.0),
                      ),
                      new Container(
                        child:  new Column(
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                  new Text(
                                    "Forecast",
                                    style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)
                                  )
                              ],
                            ),
                            new Row(
                              children: <Widget>[
                               printPredictionPrice(shortlist.price)
                              ],
                            ),
                          
                          ],
                        ),
                      ),
                      new Container(
                        height: 55.0,
                        width: 6.0,
                        color: Colors.transparent,
                        margin: const EdgeInsets.only(left: 8.0, right: 8.0),
                      ),
                      new Container(
                        child:  new Column(
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                  new Text(
                                    "Sentiment",
                                    style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)
                                  )
                              ],
                            ),
                            new Row(
                              children: <Widget>[
                                getColorCodeSentiment(shortlist.sentiment)
                              ],
                            )
                            
                          ],
                        )
                      ),
                     
                    ],
                  ),
                  new Container(
                    margin: const EdgeInsets.only(top: 5.00, bottom: 5.00),
                  ),
                
                //   new ButtonTheme.bar(
                //     child: new ButtonBar(
                //       children: <Widget>[
                //         new FlatButton(
                //           child: const Text('BUY TICKETS'),
                //           onPressed: () { /* ... */ },
                //       ),
                //       new FlatButton(
                //         child: const Text('LISTEN'),
                //         onPressed: () { /* ... */ },
                //       ),
                //     ],
                //   ),
                // )
                ]
              )
            )
          );
        }
        return Container();
      }
    );
  }

}


class NavigationIconView{
  NavigationIconView({
    Widget icon,
    String title,
    Color color,
    TickerProvider vsync,
  }) : _icon = icon,
       _color = color,
       _title = title,
       item = new BottomNavigationBarItem(
         icon: icon,
         title: new Text(title),
         backgroundColor: color,
       ),
       controller = new AnimationController(
         duration: kThemeAnimationDuration,
         vsync: vsync,
       ) {
    _animation = new CurvedAnimation(
      parent: controller,
      curve: const Interval(0.5, 1.0, curve: Curves.fastOutSlowIn),
    );
  }

  final Widget _icon;
  final Color _color;
  final String _title;
  final BottomNavigationBarItem item;
  final AnimationController controller;
  CurvedAnimation _animation;

  FadeTransition transition(BottomNavigationBarType type, BuildContext context) {
    Color iconColor;
    if (type == BottomNavigationBarType.shifting) {
      iconColor = _color;
    } else {
      final ThemeData themeData = Theme.of(context);
      iconColor = themeData.brightness == Brightness.light
          ? themeData.primaryColor
          : themeData.accentColor;
    }

    return new FadeTransition(
      opacity: _animation,
      child: new SlideTransition(
        position: new Tween<Offset>(
          begin: const Offset(0.0, 0.02), // Slightly down.
          end: Offset.zero,
        ).animate(_animation),
        child: new IconTheme(
          data: new IconThemeData(
            color: iconColor,
            size: 120.0,
          ),
          child: new Semantics(
            label: 'Placeholder for $_title tab',
            child: _icon,
          ),
        ),
      ),
    );
  }
  
     
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin{
  int _counter = 0;
  BottomNavigationBarType _type = BottomNavigationBarType.fixed;
  TabController tabController;
  List<NavigationIconView> _navigationViews;
  List<Shortlist> _shortlists = [];
  
  void initState(){
    super.initState();
    // tabController = new TabController(length: 3,vsync: this);
    _navigationViews = <NavigationIconView>[
      new NavigationIconView(
        icon: const Icon(Icons.home),
        title: 'Home',
        color: Colors.blueGrey,
        vsync: this,
      ),
      new NavigationIconView(
        icon: const Icon(Icons.trending_up),
        title: 'Trending',
        color: Colors.blueGrey,
        vsync: this,
      ),
      new NavigationIconView(
        icon: const Icon(Icons.settings),
        title: 'Settings',
        color: Colors.blueGrey,
        vsync: this,
      ),
    
    ];

    for (NavigationIconView view in _navigationViews)
      view.controller.addListener(_rebuild);

    _navigationViews[_counter].controller.value = 1.0;
  }

  void _rebuild() {
    setState(() {
      // Rebuild in order to animate views.
      
    });
  }

  @override
  void dispose() {
    for (NavigationIconView view in _navigationViews)
      view.controller.dispose();
    super.dispose();
  }
  
  
  Widget _buildTransitionsStack() {
    final List<FadeTransition> transitions = <FadeTransition>[];

    for (NavigationIconView view in _navigationViews)
      transitions.add(view.transition(_type, context));

    // We want to have the newly animating (fading in) views on top.
    transitions.sort((FadeTransition a, FadeTransition b) {
      final Animation<double> aAnimation = a.opacity;
      final Animation<double> bAnimation = b.opacity;
      final double aValue = aAnimation.value;
      final double bValue = bAnimation.value;
      return aValue.compareTo(bValue);
    });

    return new Stack(children: transitions);
  }

  _getDrawerItemWidget(int pos, List<Shortlist> _shortlists) {
    switch (pos) {
      case 0:
        return new ShortlistList(_shortlists);
      case 1:
        return new search.Search();
      case 2:
        return new settings.Settings();

      default:
        return new Text("Error");
    }
  }


  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
   
    _shortlists = <Shortlist> [
       Shortlist(change: 0.05, companyName: "General Electric Co.", ticker: "GE", price: 100.00, sentiment: 0.59),
       Shortlist(change: 0.05, companyName: "Amazon.com Inc", ticker: "AMZN", price: 100.00, sentiment: 0.59)
    ];

    final BottomNavigationBar botNavBar = new BottomNavigationBar(
      items: _navigationViews
          .map((NavigationIconView navigationView) => navigationView.item)
          .toList(),
      currentIndex: _counter,
      type: _type,
      onTap: (int index) {
        setState(() {
          _navigationViews[_counter].controller.reverse();
          _counter = index;
          _navigationViews[_counter].controller.forward();
        
        });
      },
    );

    return new Scaffold(
     
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text(widget.title),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.search),
            onPressed: (){
              Navigator.of(context).pushNamed("/search");
            },
          )
        ],
      ),
      body: _getDrawerItemWidget(_counter, _shortlists),
      // floatingActionButton: new FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: new Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
      // should be under schafold
      bottomNavigationBar: botNavBar
      
    );
  }
}
