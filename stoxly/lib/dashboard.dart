import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'dart:math' as math;
import 'package:intl/intl.dart';


class Dashboard extends StatefulWidget{
  Dashboard({Key key}) : super(key: key);
  @override
  _Dashboard createState() => new _Dashboard();
  
}

class _InputDropdown extends StatelessWidget {
  const _InputDropdown({
    Key key,
    this.child,
    this.labelText,
    this.valueText,
    this.valueStyle,
    this.onPressed }) : super(key: key);

  final String labelText;
  final String valueText;
  final TextStyle valueStyle;
  final VoidCallback onPressed;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return new InkWell(
      onTap: onPressed,
      child: new InputDecorator(
        decoration: new InputDecoration(
          labelText: labelText,
        ),
        baseStyle: valueStyle,
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Text(valueText, style: valueStyle),
            new Icon(Icons.arrow_drop_down,
              color: Theme.of(context).brightness == Brightness.light ? Colors.grey.shade700 : Colors.white70
            ),
          ],
        ),
      ),
    );
  }
}

class _DateTimePicker extends StatelessWidget {
  const _DateTimePicker({
    Key key,
    this.labelText,
    this.selectedDate,
    this.selectedTime,
    this.selectDate,
  }) : super(key: key);

  final String labelText;
  final DateTime selectedDate;
  final TimeOfDay selectedTime;
  final ValueChanged<DateTime> selectDate;

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: new DateTime(2015, 8),
      lastDate: new DateTime(2101)
    );
    if (picked != null && picked != selectedDate)
      selectDate(picked);
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle valueStyle = Theme.of(context).textTheme.title;
    return new Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        new Expanded(
          flex: 4,
          child: new _InputDropdown(
            labelText: labelText,
            valueText: new DateFormat.yMMMd().format(selectedDate),
            valueStyle: valueStyle,
            onPressed: () { _selectDate(context); },
          ),
        ),
       
      ],
    );
  }
}

class Spinner extends StatelessWidget{
  @override
  Widget build(BuildContext context){
     return new Container(
        child: Center(
          child: new CircularProgressIndicator(),
        )
      ); 
  }
}
/// Sample time series data type.
class TimeSeriesStocks {
  final DateTime time;
  final double price;

  TimeSeriesStocks(this.time, this.price);
}

class _Dashboard extends State<Dashboard>{

  DateTime _fromDate = DateTime.parse("2018-02-07");
  TimeOfDay _fromTime = const TimeOfDay(hour: 7, minute: 28);
  DateTime _toDate = new DateTime.now();
  TimeOfDay _toTime = const TimeOfDay(hour: 7, minute: 28);
  bool error = false;
  String errorMessage;
  bool updated = false;
  double close;
  double pClose;
  double sentiment;
  int positives;
  int negatives;
  double return_;
  double aReturn; 

  /// Create one series with sample hard coded data.
  List<charts.Series<TimeSeriesStocks, DateTime>> _createSampleData() {
    final data = [
      new TimeSeriesStocks(new DateTime(2017, 9, 19), 100.0),
      new TimeSeriesStocks(new DateTime(2017, 9, 26), 101.0),
      new TimeSeriesStocks(new DateTime(2017, 10, 3), 99.0),
      new TimeSeriesStocks(new DateTime(2017, 10, 10), 98.7),
    ];

    return [
      new charts.Series<TimeSeriesStocks, DateTime>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesStocks sales, _) => sales.time,
        measureFn: (TimeSeriesStocks sales, _) => sales.price,
        data: data,
      )
    ];
  }

  Future<http.Response> getDemo() async{
    String accessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjY1NDU3MjQsIm5iZiI6MTUyNjU0NTcyNCwianRpIjoiODBhZGY1NGEtNDk4ZC00YTJkLTg3MDMtMWM5YWQ3NjlmYmZlIiwiZXhwIjoxNTI5MTM3NzI0LCJpZGVudGl0eSI6IjI1OWNhN2QxNzViNTQxNWViYjlhODFlYzA4ODE4M2Y5IiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIiwiY3NyZiI6IjBhMjU1MzY2LTEwNmMtNGMzOC1hOTE5LWU4MDYyODM1ZDJmZiJ9.twl7lneVYXWf_3P2ZziLyz2L_xDb6KmAaYoUQzgYhE8";

    http.Response response = await http.get(
      "http://35.189.14.118/demo",
      headers: {
        "Accept":"application/json",
        "Authorization": accessToken
      }
    );
    print ("demo");
    print (JSON.decode(response.body));
    return response;
  }

  Future<http.Response> changeDate(String date) async{
    String accessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjY1NDU3MjQsIm5iZiI6MTUyNjU0NTcyNCwianRpIjoiODBhZGY1NGEtNDk4ZC00YTJkLTg3MDMtMWM5YWQ3NjlmYmZlIiwiZXhwIjoxNTI5MTM3NzI0LCJpZGVudGl0eSI6IjI1OWNhN2QxNzViNTQxNWViYjlhODFlYzA4ODE4M2Y5IiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIiwiY3NyZiI6IjBhMjU1MzY2LTEwNmMtNGMzOC1hOTE5LWU4MDYyODM1ZDJmZiJ9.twl7lneVYXWf_3P2ZziLyz2L_xDb6KmAaYoUQzgYhE8";

    http.Response response = await http.get(
      "http://35.189.14.118/demo?date=$date",
      headers: {
        "Accept":"application/json",
        "Authorization": accessToken
      }
    );
    print ("demo");
    print (JSON.decode(response.body));
    return response;
  }


  MaterialColor getColorCode(double value){
    if(value<=0){
      return Colors.red;
    }else{
      return Colors.green;
    }
  }
 

  fillInStats(String date, BuildContext context){
     
    Future<http.Response> res = changeDate(date);
    res.then((result) {

      if (result!=null){
        updated = true;
        int code = result.statusCode;
        if (code == 400){
          Map msgFeedback = JSON.decode(result.body);
          String msg = msgFeedback["message"];

          error = true;
          errorMessage = msg;
          if (error){
            var snackBar = new SnackBar(
              content: new Text(errorMessage),
              action: null
            );    

            Scaffold.of(context).showSnackBar(snackBar); 

          }

        }else{
          error = false;
          Map data = JSON.decode(result.body);
          close = data["close"];
          close = (num.parse(close.toStringAsFixed(2))).toDouble();
          pClose = data["prediction_price"];
          pClose = (num.parse(pClose.toStringAsFixed(2))).toDouble();
          sentiment = data["aggregate_sentiment"];
          sentiment = (num.parse(sentiment.toStringAsFixed(2))).toDouble();
          positives = data["positives"];
          negatives = data["negatives"];
          return_ = data["prediction_return"];
          return_ = (num.parse(return_.toStringAsFixed(6))).toDouble();
          aReturn = data["return"];
          aReturn = (num.parse(aReturn.toStringAsFixed(4))).toDouble();
         
        }
      }

      
      return new Container();
    });
  }

  List<charts.Series<TimeSeriesStocks, DateTime>> _chartData(List chartData) {
    List<TimeSeriesStocks> data = [];
    for (Map d in chartData){
      data.add(new TimeSeriesStocks(DateTime.parse(d["date"]), d["close"]));
    }
  
    return [
      new charts.Series<TimeSeriesStocks, DateTime>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (TimeSeriesStocks sales, _) => sales.time,
        measureFn: (TimeSeriesStocks sales, _) => sales.price,
        data: data,
      )
    ];
  }

  
  @override
  Widget build(BuildContext context) {
   
    return new Scaffold(
      appBar: new AppBar(title: const Text('Dashboard (Demo)')),
      body: new FutureBuilder(
        future: getDemo(),
        builder: (BuildContext context, AsyncSnapshot<http.Response> res){
          if (res.hasData){
            Map data = JSON.decode(res.data.body);
            List charts_ = data["charts"];
            List<charts.Series<TimeSeriesStocks, DateTime>> chartData = _chartData(charts_);
            if (!updated){
               close = data["close"];
              close = (num.parse(close.toStringAsFixed(2))).toDouble();
              pClose = data["prediction_price"];
              pClose = (num.parse(pClose.toStringAsFixed(2))).toDouble();
              sentiment = data["aggregate_sentiment"];
              sentiment = (num.parse(sentiment.toStringAsFixed(2))).toDouble();
              positives = data["positives"];
              negatives = data["negatives"];
              return_ = data["prediction_return"];
              return_ = (num.parse(return_.toStringAsFixed(6))).toDouble();
              aReturn = data["return"];
              aReturn = (num.parse(aReturn.toStringAsFixed(4))).toDouble();
            }
           

            return new DropdownButtonHideUnderline(
              child: new SafeArea(
                top: false,
                bottom: false,
                child: new ListView(
                  padding: const EdgeInsets.all(16.0),
                  children: <Widget>[
                  
                    new _DateTimePicker(
                      labelText: 'Date',
                      selectedDate: _fromDate,
                      selectedTime: _fromTime,
                      selectDate: (DateTime date) {
                        setState(() {
                          _fromDate = date;
                         
                          var formatter = new DateFormat('yyyy-MM-dd');
                          String dateFormatted = formatter.format(date);
                          fillInStats(dateFormatted, context);
                          
                        });
                      },
                
                    ),
                    new Padding(
                      padding: EdgeInsets.all(15.0),
                    ),
                    new Container(
                      child: new Text("Price Prediction", style: new TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600, color: Colors.blueGrey),),
                    ),
                    new Padding(
                      padding:  EdgeInsets.all(10.0),
                    ),
                    
                    new Container(
                      padding: EdgeInsets.all(1.0),
                      child: new charts.TimeSeriesChart(chartData,
                        animate: true,
                        // Optionally pass in a [DateTimeFactory] used by the chart. The factory
                        // should create the same type of [DateTime] as the data provided. If none
                        // specified, the default creates local date time.
                        dateTimeFactory: const charts.LocalDateTimeFactory(),
                      ),
                      height: 200.0,
                      width:350.0
                    ),
                    new Padding(
                      padding: EdgeInsets.all(10.0),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment:  CrossAxisAlignment.start,
                      children: <Widget>[
                        
                        new Column(
                          crossAxisAlignment:CrossAxisAlignment.start,
                          // mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: <Widget>[new Text("Predicted Price", style: new TextStyle(color:Colors.black,fontWeight: FontWeight.bold, fontSize: 20.0),)],),
                            new Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text("\$"+pClose.toString(), style: new TextStyle(color:Colors.black, fontWeight: FontWeight.w300, fontSize: 20.0)),
                              ],
                            )
                          ],
                        ),
                        new Padding(padding: EdgeInsets.all(8.0),),
                        new Column(
                          crossAxisAlignment:CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            new Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[new Text("Predicted Return", style: new TextStyle(color:Colors.black,fontWeight: FontWeight.bold, fontSize: 20.0),)],),
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(return_.toString(), style: new TextStyle(color:getColorCode(return_), fontWeight: FontWeight.w300, fontSize: 20.0)),
                              ],
                            )
                          ],
                        )
                    
                    ]),
                    new Padding(padding: EdgeInsets.all(5.0)),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment:MainAxisAlignment.start,

                          children: <Widget>[
                            new Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: <Widget>[new Text("Actual Price", style: new TextStyle(color:Colors.black,fontWeight: FontWeight.bold, fontSize: 20.0),)],),
                            new Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text("\$"+close.toString(), style: new TextStyle(color:Colors.black, fontWeight: FontWeight.w300, fontSize: 20.0)),
                            ],
                          ),
                        ]),
                        new Padding(
                          padding: EdgeInsets.all(23.0),
                        ),
                        new Column( 
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment:MainAxisAlignment.start,

                          children: <Widget>[
                            new Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: <Widget>[new Text("Actual Return", style: new TextStyle(color:Colors.black,fontWeight: FontWeight.bold, fontSize: 20.0),)],),
                            new Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text(aReturn.toString(), style: new TextStyle(color:getColorCode(aReturn), fontWeight: FontWeight.w300, fontSize: 20.0)),
                              ],
                            )
                          ],
                        ),
                    
                      ]
                    ),
                    new Padding(
                      padding: EdgeInsets.all(5.0),
                    ),
                     new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment:MainAxisAlignment.start,

                          children: <Widget>[
                            new Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: <Widget>[new Text("Absolute Error of Return", style: new TextStyle(color:Colors.black,fontWeight: FontWeight.bold, fontSize: 20.0),)],),
                            new Row(
                              crossAxisAlignment:CrossAxisAlignment.start,
                              children: <Widget>[
                                new Text((return_- aReturn).abs().toStringAsFixed(4), style: new TextStyle(color:Colors.black, fontWeight: FontWeight.w300, fontSize: 20.0)),
                            ],
                          ),
                        ]),
                        new Padding(
                          padding: EdgeInsets.all(23.0),
                        ),
                    
                      ]
                    ),
                    new Padding(
                      padding: EdgeInsets.all(10.0)
                    ),
                    new Container(
                      child: new Text("Aggregate Sentiment", style: new TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600, color: Colors.blueGrey),),
                    ),
                    new Padding(
                      padding:  EdgeInsets.all(10.0),
                    ),
                    // new Container(
                    //   padding: EdgeInsets.all(1.0),
                    //   child: new charts.TimeSeriesChart(_createSampleData(),
                    //     animate: true,
                    //     // Optionally pass in a [DateTimeFactory] used by the chart. The factory
                    //     // should create the same type of [DateTime] as the data provided. If none
                    //     // specified, the default creates local date time.
                    //     dateTimeFactory: const charts.LocalDateTimeFactory(),
                    //   ),
                    //   height: 200.0,
                    //   width:350.0
                    // ),
                    // new Padding(
                    //   padding: EdgeInsets.all(10.0),
                    // ),
                    new Row(
                      
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Column(
                          
                          children: <Widget>[
                            new Row(
                              children: <Widget>[
                                new Text("Positive", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
                                
                              ],
                            ),
                            new Row(children: <Widget>[
                              new Text(positives.toString(), style: TextStyle( fontSize: 20.0),)
                            ],)
                            
                          ],
                          
                        ),
                        new Padding(padding: EdgeInsets.all(2.0),),
                        new Column(children: <Widget>[
                          new Row(
                            // crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Column(
                                children: <Widget>[
                                  new Row(
                                    children: <Widget>[
                                      new Text("Negative", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
                                      
                                    ],
                                  ),
                                  new Row(children: <Widget>[
                                    new Text(negatives.toString(), style: TextStyle(fontSize: 20.0),)
                                  ],)
                                  
                                ],
                              ),
                            ],
                          ),
                        ],),
                        new Padding(
                          padding: EdgeInsets.all(2.0),
                        ),
                        new Column(
                          children: <Widget>[
                            new Row(
                              // crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                new Column(
                                  children: <Widget>[
                                    new Row(
                                      children: <Widget>[
                                        new Text("Score", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
                                      ],
                                    ),
                                    new Row(children: <Widget>[
                                      new Text(sentiment.toString(), style: TextStyle(fontSize: 20.0, color: getColorCode(sentiment)),)
                                    ],)
                                    
                                  ],
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                    new Padding(
                      padding: EdgeInsets.all(10.0),
                    ),
                  
                  
                  ],
                ),
              ),
            );
          }else{
            return new Spinner();
          }
        }
      ),
     
    );
  }
}