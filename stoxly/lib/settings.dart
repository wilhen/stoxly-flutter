import 'package:flutter/material.dart';

class Settings extends StatelessWidget{
  
  @override
  Widget build(BuildContext context) {
 
    return new Scaffold(
      
      body: Container(
        child: 
        new ListView(
          children: <Widget>[
            new GestureDetector(
                child: new ListTile(
                  leading: new Icon(Icons.dashboard),
                  title: new Text("Dashboard", style: new TextStyle(fontWeight: FontWeight.w500)),
                  subtitle: new Text("Demo"),
                
                ),
                onTap: (){
                   Navigator.of(context).pushNamed("/dashboard");
                },
            ),
            new ListTile(
              leading: new Icon(Icons.power_settings_new),
              title: new Text("Logout", style: new TextStyle(fontWeight: FontWeight.w500)),
            
            )
          ],
        )
      ),
    );
  }
}